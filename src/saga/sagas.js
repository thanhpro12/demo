import { takeEvery, call, put } from "redux-saga/effects";
export default function* watcherSaga() {
    yield takeEvery("FETCH_DATA", workerSaga);
}
function* workerSaga() {
    try {
        const payload = yield call(getData);
        yield put({ type: "DATA_LOADED", payload });
    } catch (e) {
        yield put({ type: "API_ERRORED", payload: e });
    }
}
function getData() {
    return fetch('https://5d2fe6a028465b00146aa991.mockapi.io/api/fonts')
    .then(res => res.json())
}
