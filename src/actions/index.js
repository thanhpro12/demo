import {FETCH_DATA,TOGGLE_FILTER,CHECK_CATEGORY,SEARCH_FONT,CHANGE_CONTENT,CHANGE_CONTENT_SELECT,CHANGE_SIZE,SHOW_PRODUCT,CHANGE_SIZE_VALUE} from '../constant'
//PRODUCTGRID COMPONENT
export const fetchData=()=>({
	type:"FETCH_DATA"
})

//FILTER COMPONENT
export const toggleFilter=(name)=>({
	type:"TOGGLE_FILTER",
	name
})
export const checkCategory=(values)=>({
	type:"CHECK_CATEGORY",
	values
})
export const checkBoxCategory=(values)=>({
	type:"CHECK_BOX_CATEGORY",
	values
})
export const changeLanguage=(value)=>({
	type:"CHANGE_LANGUAGE",
	value
})
export const changeSort=(value)=>({
	type:"CHANGE_SORT",
	value
})
//SEARCH COMPONENT
export const searchFont=(name)=>({
	type:"SEARCH_FONT",
	name
})
export const changeContent=(content)=>({
	type:"CHANGE_CONTENT",
	content
})
export const changeContentSelect=(value)=>({
	type:"CHANGE_CONTENT_SELECT",
	value
})
export const changeSize=(value)=>({
	type:"CHANGE_SIZE",
	value
})
export const showProduct=()=>({
	type:"SHOW_PRODUCT",
})
export const changeSizeValue=(value)=>({
	type:"CHANGE_SIZE_VALUE",
	value
})
//PRODUCT COMPONENT
export const changeContentID=(ID,value)=>({
	type:"CHANGE_CONTENT_ID",
	value,
	ID
})
export const changeSizeID=(ID,value)=>({
	type:"CHANGE_SIZE_ID",
	value,
	ID
})
export const AddID=(ID)=>({
	type:"ADD_ID",
	ID
})
//FONT-CART COMPONENT
export const clearAllFontCart=()=>({
	type:"CLEAR_ALL_FONT_CART"
})
export const deleteFontID=(ID)=>({
	type:"DELETE_FONT_ID",
	ID
})
export const loadFontCart=(fontCartList)=>({
	type:"LOAD_FONT_CART",
	fontCartList
})