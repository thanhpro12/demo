import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';

export default class PersonDetail extends Component {
  constructor(props) {
      super(props);
  }
  onClickDelete=(e,index)=>{
    this.props.onDelete(this.props.index);
  }
  onClickUpdate=(e,index)=>{
    this.props.onUpdate(this.props.index);
  }
  
  render() {
    return (
      <tr>
        <th scope="row">{this.props.index}</th>
        <td>{this.props.person.full_name}</td>
        <td>{this.props.person.email}</td>
        <td>{this.props.person.phone_number}</td>
        <td><i className="fa fa-pencil-square-o fa-2x icon_update"></i><i className="fa fa-window-close fa-2x icon_delete" onClick={this.onClickDelete}></i></td>
      </tr>
   );
}
}