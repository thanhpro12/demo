import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';
import Search from './components/Search';
import Filter from './components/Filter';
import NavBar from './components/NavBar';
import ProductGrid from './components/ProductGrid';
import FontCart from './components/FontCart';
import { connect } from 'react-redux';

export default class App extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="App">
        <div className="wrap_content">
          <NavBar />
          <Search />
          <Filter />
          <ProductGrid />
          <FontCart/>
        </div>
      </div>

    );
  }
}