const fontList = []
const content = {
	Sentence: 'A red flare silhouetted the jagged edge of a wing',
	Alphabet: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*',
	Paragraph: 'A peep at some distant orb has power to raise and purify our thoughts like a strain of sacred music, or a noble picture, or a passage from the grander poets. It always does one good.',
	Numerals: '1234567890',
	Custom: ''
}
export const fontReducer = (state = fontList, action) => {
	switch (action.type) {
		case "DATA_LOADED": {
			console.log('language:')
			console.log([...new Set([].concat(...action.payload.map(font => font.subsets)))])
			return action.payload.map(font => { return { ...font, visualName: true, visualCategory: true, visualLanguage: true, content: content.Sentence, fontSize: 32, isAdding: false } })

		}
		case "CHECK_CATEGORY":
			// console.log(state.filter(font =>!action.values.includes(font.category)))
			return state.map(font => action.values.find((value) => value === font.category) ? { ...font, visualCategory: true } : { ...font, visualCategory: false })
		case "SEARCH_FONT":
			return state.map(font => font.family.toLowerCase().indexOf(action.name) !== -1 ? { ...font, visualName: true } : { ...font, visualName: false })
		case "CHANGE_CONTENT":
			return state.map(font => { return { ...font, content: action.content } })
		case "CHANGE_CONTENT_SELECT":
			return state.map(font => { return { ...font, content: content[action.value] } })
		case "CHANGE_SIZE":
			return state.map(font => { return { ...font, fontSize: action.value } })
		case "CHANGE_SORT":
			switch (action.value) {
				case 'trending':
					return [...state].sort((a, b) => a.trending > b.trending ? 1 : -1)
				case 'popular':
					return [...state].sort((a, b) => a.popularity > b.popularity ? 1 : -1)
				case 'newest':
					return [...state].sort((a, b) => new Date(a.lastModified) > new Date(b.lastModified) ? 1 : -1)
				case 'name':
					return [...state].sort((a, b) => a.family.localeCompare(b.family))
			}

		case "CHANGE_LANGUAGE":
			if (action.value === 'allLanguage')
				return state.map(font => { return { ...font, visualLanguage: true } })
			return state.map(font => font.subsets.find((value) => value === action.value) ? { ...font, visualLanguage: true } : { ...font, visualLanguage: false })
		case "CHANGE_CONTENT_ID":
			return state.map((font, index) => index == action.ID ? { ...font, content: content[action.value] } : font)
		case "CHANGE_SIZE_ID":
			return state.map((font, index) => index == action.ID ? { ...font, fontSize: action.value } : font)
		case "ADD_ID":
			return state.map((font, index) => index == action.ID ? { ...font, isAdding: !font.isAdding } : font)
		case "CLEAR_ALL_FONT_CART":
			return state.map((font, index) => { return { ...font, isAdding: false } })
		case "DELETE_FONT_ID":
			return state.map((font, index) => index == action.ID ? { ...font, isAdding: false } : font)
		case "LOAD_FONT_CART":
			return state.map((font, index) =>{return { ...font, isAdding: action.fontCartList[index] }})
		default:
			return state
	}
}