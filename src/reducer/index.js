import { combineReducers } from 'redux'
import {fontReducer} from './font'
import {visualReducer} from './visual'

export default combineReducers({
  visualReducer,
  fontReducer
})
