const visual = {
	isDisplayCategory: false,
	isDisplayProperty: false,
	isAppear: false,
	sizeValue: 32,
	checkBoxCategory: ['Serif', "Sans Serif", "Display", "Handwriting", "Monospace"]
}
export const visualReducer = (state = visual, action) => {
	switch (action.type) {
		case "TOGGLE_FILTER":
			return { ...state, [action.name]: !state[action.name] }
		case "SHOW_PRODUCT":
			return { ...state, isAppear: true }
		case "CHANGE_SIZE_VALUE":
			return { ...state, sizeValue: action.value }
		case "CHECK_BOX_CATEGORY":
			return { ...state,checkBoxCategory: action.values }
		default:
			return state
	}
}