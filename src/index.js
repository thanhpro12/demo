import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from 'react-redux'
import rootReducer from './reducer';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import createSagaMiddleware from 'redux-saga';
import apiSaga from "./saga/sagas.js";

// Create store

const initialiseSagaMiddleware = createSagaMiddleware();
const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer,storeEnhancers(
    applyMiddleware( initialiseSagaMiddleware)
  ));
initialiseSagaMiddleware.run(apiSaga);
const appRoot = (
    <Provider store={store}>
        <div>
            <App />
        </div>
    </Provider>
)

ReactDOM.render(appRoot, document.getElementById('root'))