import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Select } from 'antd';
import 'antd/dist/antd.css';
import { Slider } from 'antd';
import { Checkbox } from 'antd';
import { toggleFilter} from '../actions'
import { checkCategory} from '../actions'
import { checkBoxCategory} from '../actions'
import { changeLanguage} from '../actions'
import { changeSort} from '../actions'
const { Option } = Select;
const mapStateToProps = state => {
    return {
        isDisplayCategory: state.visualReducer.isDisplayCategory,
        isDisplayProperty:state.visualReducer.isDisplayProperty,
        fonts:state.fontReducer,
        checkedCategory:state.visualReducer.checkBoxCategory
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        toggleFilter : (name) => {
            dispatch(toggleFilter(name));
        },
        checkCategory : (values) => {
            dispatch(checkCategory(values));
        },
        checkBoxCategory : (values) => {
            dispatch(checkBoxCategory(values));
        },
        changeLanguage : (value) => {
            dispatch(changeLanguage(value));
        },
        changeSort : (value) => {
            dispatch(changeSort(value));
        },
    }
}
class Filter extends Component {
    constructor(props) {
        super(props);
        this.state={
            checkedPropertyValues:[]
        }
    }
    onClickCategory = (e) => {
        this.props.toggleFilter('isDisplayCategory')
    }
    onClickProperty = (e) => {
        this.props.toggleFilter('isDisplayProperty')
    }
    onChangeCheckboxCategory = (checkedValues) => {
        this.props.checkCategory(checkedValues)
        this.props.checkBoxCategory(checkedValues)
    }
    onChangeCheckboxProperty = (checkedValues) => {
        this.setState({ checkedPropertyValues:checkedValues })
    }
    onChangeDropLanguage=(value)=>{
        this.props.changeLanguage(value)
    }
    onChangeSort=(value)=>{
        this.props.changeSort(value)
    }
    render() {
        let isDisplayCategory = this.props.isDisplayCategory;
        let isDisplayProperty = this.props.isDisplayProperty;
        let checkedPropertyValues=this.state.checkedPropertyValues;
        let checkedCategory=this.props.checkedCategory
        let fontsNumber=this.props.fonts.length
        let fontsFilter=this.props.fonts.reduce((sum,font)=>(font.visualName&&font.visualCategory&&font.visualLanguage)?(sum+1):sum,0)
        let listLanguages=["menu", "latin", "latin-ext", "sinhala", "greek", "hebrew", "cyrillic", "cyrillic-ext", "greek-ext", "vietnamese", "devanagari", "arabic", "khmer", "tamil", "thai", "bengali", "gujarati", "oriya", "malayalam", "gurmukhi", "kannada", "telugu"]
        return (<>
                <div className="filter-wrapper">
                    <div className="category-wrapper">
                        <div className="category-button" id="category-button" onClick={this.onClickCategory}>
                            Categories
                        </div>
                        {isDisplayCategory && <div className="category-dropout">
                            Category:
                            <Checkbox.Group style={{ width: '100%', 'text-align': 'left' }} defaultValue={['Serif', "Sans Serif", "Display", "Handwriting", "Monospace"]} value={checkedCategory} onChange={this.onChangeCheckboxCategory}>
                                <Checkbox style={{ display: 'block', 'margin-left': '8px', 'margin-bottom': '5px' }} value="Serif">Serif</Checkbox>
                                <Checkbox style={{ display: 'block', 'margin-bottom': '5px' }} value="Sans Serif">San Serif</Checkbox>
                                <Checkbox style={{ display: 'block', 'margin-bottom': '5px' }} value="Display">Display</Checkbox>
                                <Checkbox style={{ display: 'block', 'margin-bottom': '5px' }} value="Handwriting">HandWriting</Checkbox>
                                <Checkbox style={{ display: 'block', 'margin-bottom': '5px' }} value="Monospace">Monospace</Checkbox>
                            </Checkbox.Group>
                        </div>}
                    </div>
                    {}
                    <div className="option-wrapper">
                        <Select style={{ width: '140px' }} className="category" defaultValue="allLanguage" onChange={this.onChangeDropLanguage}>
                            <Option value="allLanguage">All Language</Option>
                            {listLanguages.map(language=><Option value={language}>{language}</Option>)}
                        </Select>
                    </div>
                    <div className="property-wrapper">
                        <div className="property-button" id="property-button" onClick={this.onClickProperty}>
                            Property
                        </div>
                        {isDisplayProperty && <div className="property-dropout">
                            Font Property:
                            <Checkbox.Group style={{ width: '100%', 'margin-left': '8px', 'text-align': 'left' }} onChange={this.onChangeCheckboxProperty}>
                                <div>Number of styles </div>
                                <Checkbox style={{ display: 'block', 'margin-bottom': '5px' }} value="Styles"></Checkbox><div className="slider-wrapper"><Slider disabled={checkedPropertyValues.indexOf("Styles")===-1} style={{width:200,'margin-top':-20,'margin-left':28}} defaultValue={32} max="248" /></div>
                                <div>Thickness </div>
                                <Checkbox style={{ display: 'block', 'margin-bottom': '5px' }} value="Thickness"></Checkbox><div className="slider-wrapper"><Slider disabled={checkedPropertyValues.indexOf("Thickness")===-1} style={{width:200,'margin-top':-20,'margin-left':28}} defaultValue={32} max="248" /></div>
                                <div>Slant </div>
                                <Checkbox style={{ display: 'block', 'margin-bottom': '5px' }} value="Slant"></Checkbox><div className="slider-wrapper"><Slider disabled={checkedPropertyValues.indexOf("Slant")===-1} style={{width:200,'margin-top':-20,'margin-left':28}} defaultValue={32} max="248" /></div>
                                <div>Width </div>
                                <Checkbox style={{ display: 'block', 'margin-bottom': '5px' }} value="Width"></Checkbox><div className="slider-wrapper"><Slider disabled={checkedPropertyValues.indexOf("Width")===-1} style={{width:200,'margin-top':-20,'margin-left':28}} defaultValue={32} max="248" /></div>
                            </Checkbox.Group>
                        </div>}
                    </div>

                </div>
                <div className="sort-wrapper">
                    <div className="static">
                        Viewing {fontsFilter} of {fontsNumber} font families
            </div>
                    <div className="sort">
                        SORT BY:
                <Select style={{ width: '140px', 'margin-left': '10px' }} className="category" defaultValue="trending" onChange={this.onChangeSort}>
                            <Option value="trending">Trending</Option>
                            <Option value="popular">Popular</Option>
                            <Option value="newest">Newest</Option>
                            <Option value="name">Name</Option>
                        </Select>
                    </div>
                </div>
            </>
        );
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Filter);