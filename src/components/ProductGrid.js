import React, { Component } from 'react'
import { connect } from 'react-redux';
import { fetchData } from '../actions'
import {showProduct} from '../actions'
import { Row, Col } from 'antd';
import { Select } from 'antd';
import 'antd/dist/antd.css';
import { Slider } from 'antd';
import { Input } from 'antd';
import { Spin } from 'antd';
import Product from './Product'
const { Option } = Select;
const { TextArea } = Input;
const mapStateToProps = state => {
  return {
    fontList: state.fontReducer,
    isAppear:state.visualReducer.isAppear
  };
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchData: () => {
      dispatch(fetchData());
    },
    showProduct: () => {
      dispatch(showProduct());
    }
  }
}
class ProducGrid extends Component {
  constructor(props) {
    super(props)
  }
  componentWillMount = () => {
    console.log('fetch data...')
    this.props.fetchData()
  }
  componentDidUpdate=(prevProps, prevState, snapshot)=>{
    if(!this.props.isAppear)
      this.props.showProduct()
  }
  handleChangePreview = (value) => {
  }
  render() {
    let fontList = this.props.fontList
    if(fontList.length==0)
      return <> <Spin tip="Loading..." style={{'margin-top':'100px'}} size="large" /> </>
    return <>
    <div className="product-wrapper">
      <Row>
        {fontList.map((font,index)=><Product font={font} index={index} />)}
      </Row>
    </div>
    </>
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProducGrid);
