import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deleteFontID } from '../actions'

const mapDispatchToProps = (dispatch, props) => {
    return {
        deleteFontID: (ID) => {
            dispatch(deleteFontID(ID));
        },
    }
}
class Font extends Component {
    constructor(props) {
        super(props);
    }
    deleteFontID=(e)=>{
        this.props.deleteFontID(this.props.index)
    }
    render() {
        let fontName=this.props.fontName
        return (<>
            <div className="font-wrapper" onClick={this.deleteFontID}>
                {fontName}<i class="fa fa-close" style={{'font-size':'24px'}}></i>
            </div>
        </>
        );
    }
}
export default connect(null, mapDispatchToProps)(Font);