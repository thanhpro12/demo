import React, { Component } from 'react';
import { connect } from 'react-redux';
import { clearAllFontCart } from '../actions'
import { loadFontCart } from '../actions'
import { Button } from 'antd';
import Font from './Font';
const mapStateToProps = state => {
    return {
        fontList: state.fontReducer,
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        clearAllFontCart: () => {
            dispatch(clearAllFontCart());
        },
        loadFontCart: (fontCartList) => {
            dispatch(loadFontCart(fontCartList));
        },
    }
}
class FontCart extends Component {
    constructor(props) {
        super(props);
        this.state = { loadLocal: false,isShowDetail:false };
    }
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        let fontCartList = JSON.parse(localStorage.getItem('fontCartList'))
        if (fontCartList && this.props.fontList.length && !this.state.loadLocal) {
            this.setState({ loadLocal: true })
            this.props.loadFontCart(fontCartList)
        }
        if (this.props.fontList.length) {
            let fontCartList = this.props.fontList.map((font) => font.isAdding)
            localStorage.setItem('fontCartList', JSON.stringify(fontCartList));
        }

    }
    onChangeClearAll = (e) => {
        this.props.clearAllFontCart()
    }
    showDetailFontCart=(e)=>{
        this.setState({isShowDetail:!this.state.isShowDetail })
    }
    render() {
        let fontCartNumber = this.props.fontList.reduce((sum, font) => (font.isAdding) ? (sum + 1) : sum, 0)
        let fontList = this.props.fontList
        let fontName=fontList.reduce((fontName,font)=>font.isAdding ?(fontName+font.family+'|'):fontName,'')
        return (<>
            <div className="font-cart-wrapper" style={{top:this.state.isShowDetail?'717px':'917px'}}>
                <div className="font-cart-header" onClick={this.showDetailFontCart}>
                    {fontCartNumber} Families Selected
                    </div>
                <div className="font-cart-content">
                <div className="font-cart-clear">
                    <Button style={{ 'background-color': 'red',color:'black' }} type="primary" onClick={this.onChangeClearAll}>Clear All</Button>
                </div>
                <div className="font-in-cart">
                    {fontList.map((font, index) => font.isAdding ? <Font fontName={font.family} index={index} /> : null)}
                </div>
                <div className="font-embedded">
                <strong>Embed Font</strong> <br />To embed your selected fonts into a webpage, copy this code into the head of your HTML document.
                </div>
                <div className="font-link">
                    {'<link href="https://fonts.googleapis.com/css?family='+fontName.split(' ').join('+').slice(0, -1)+'" rel="stylesheet">'}
                </div>
                <div className="font-embedded">
                    <strong>Specify in CSS</strong><br/>Use the following CSS rules to specify these families:
                </div>
                <div className="font-link">
                    {fontList.map((font) => font.isAdding?<>font-family: '{font.family}';<br/></>:null)}
                </div>
                </div>
            </div>
        </>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FontCart);