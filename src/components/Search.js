import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Select } from 'antd';
import 'antd/dist/antd.css';
import { Slider } from 'antd';
import { searchFont } from '../actions'
import { changeContent } from '../actions'
import { changeContentSelect } from '../actions'
import { changeSize } from '../actions'
import { changeSizeValue } from '../actions'
const { Option } = Select;


const mapStateToProps = state => {
  return {
    isDisplayCategory: state.visualReducer.isDisplayCategory,
    sizeValue: state.visualReducer.sizeValue,
  };
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    searchFont: (name) => {
      dispatch(searchFont(name));
    },
    changeContent: (content) => {
      dispatch(changeContent(content));
    },
    changeContentSelect: (value) => {
      dispatch(changeContentSelect(value));
    },
    changeSize: (value) => {
      dispatch(changeSize(value));
    },
    changeSizeValue: (value) => {
      dispatch(changeSizeValue(value));
    },
  }
}

class Search extends Component {
  constructor(props) {
    super(props);
  }
  onChangeContentSelect = (value) => {
    this.props.changeContentSelect(value)
  }
  onChangeSizeSelect = (value) => {
    this.props.changeSize(value)
    this.props.changeSizeValue(value)
  }
  onChangeSizeSlider = (value) => {
    this.props.changeSize(value)
    this.props.changeSizeValue(value)
  }
  onChangeSearch = (e) => {
    this.props.searchFont(e.target.value)
  }
  onChangeContent = (e) => {
    this.props.changeContent(e.target.value)
  }
  render() {
    let {sizeValue}=this.props
    return (
      <div className="search-wrapper">
        <div className="icon-search"><i className="fa fa-search fa-2x"></i></div>
        <input type="text" name="search" className="search-font" onChange={this.onChangeSearch} />
        <div className="border-line">
        </div>
        <div className="option-wrapper">
          <Select style={{ width: '100px', 'margin-left': '10px' }} className="filter" defaultValue="Sentence" onChange={this.onChangeContentSelect}>
            <Option value="Sentence">Sentence</Option>
            <Option value="Alphabet">Alphabet</Option>
            <Option value="Paragraph">Paragraph</Option>
            <Option value="Numerals">Numerals</Option>
            <Option value="Custom">Custom</Option>
          </Select>
        </div>
        <div className="border-line">
        </div>
        <input type="text" name="content" className="content-font" onChange={this.onChangeContent} />
        <div className="border-line">
        </div>
        <div className="option-wrapper">
          <Select style={{ width: '100px', 'margin-left': '10px' }} className="filter" defaultValue={32} onChange={this.onChangeSizeSelect} value={sizeValue}>
            <Option value={8}>8px</Option>
            <Option value={16}>16px</Option>
            <Option value={32}>32px</Option>
            <Option value={64}>64px</Option>
            <Option value={128}>128px</Option>
          </Select>
        </div>
        <div className="slider-wrapper">
          <Slider defaultValue={32} max="248" onChange={this.onChangeSizeSlider} value={sizeValue} />
        </div>
        <div className="border-line">
        </div>
        <div className="icon-color"><i class="fa fa-adjust fa-2x"></i></div>
        <div className="border-line">
        </div>
        <div className="icon-color"><i class="fa fa-th fa-2x"></i></div>
        <div className="border-line">
        </div>
        <div className="icon-color"><i class="fa fa-repeat fa-2x"></i></div>
      </div >

    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Search);