import { Component } from 'react'
import React from 'react';

export default class NavBar extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return <div>
      <div className="nav-google">
        <span className="google-image">
        </span>
        <span className="google-font">
          Fonts
        </span>
        <div className="nav-link">
          <ul>
            <li className="mg-20 hover-red">DIRECTORY</li>
            <li className="mg-20 hover-red">FEATURED</li>
            <li className="mg-20 hover-red">ARTICLE</li>
            <li className="mg-20 hover-red">ABOUT</li>
          </ul>
        </div>
      </div>
    </div>
  }
}
