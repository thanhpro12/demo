import React, { Component } from 'react'
import { Row, Col } from 'antd';
import { Select } from 'antd';
import 'antd/dist/antd.css';
import { Slider } from 'antd';
import { Input } from 'antd';
import { connect } from 'react-redux';
import { changeContentID} from '../actions'
import { changeSizeID} from '../actions'
import { AddID} from '../actions'


const { Option } = Select;
const { TextArea } = Input;
const mapStateToProps = state => {
  return {
    isAppear:state.visualReducer.isAppear
  };
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    onChangeContentID : (ID,value) => {
          dispatch(changeContentID(ID,value));
    },
    onChangeSizeSliderID : (ID,value) => {
      dispatch(changeSizeID(ID,value));
    },
    onClickAddID : (ID) => {
      dispatch(AddID(ID));
    },
  }
}
class Product extends Component {
  constructor(props) {
    super(props)
  }
  onChangeContentID = (value) => {
    this.props.onChangeContentID(this.props.index,value)
  }
  onChangeSizeSliderID = (value) => {
    this.props.onChangeSizeSliderID(this.props.index,value)
  }
  onClickAddID = (e) => {
    this.props.onClickAddID(this.props.index)
  }
  render() {
    let { font } = this.props;
    let {isAppear}= this.props
    if (!(font.visualName&&font.visualCategory&&font.visualLanguage))
      return null
    return <>
      <link href={"https://fonts.googleapis.com/css?family=" + font.family} rel="stylesheet" />
      <Col style={{ 'height': 500}} lg={{ span: 5, offset: 1 }}>
        <div style={{'opacity': isAppear?1:0}} className="product">
          <div className="header-product">
            <div className="name-font">
              {font.family}
            </div>
            <div className="author-font">
              {font.designers[0]} ( {Object.keys(font.fonts).length} styles )
              </div>
            <div className="add-cart">
              {font.isAdding?<i class="fa fa-minus-circle fa-2x" onClick={this.onClickAddID}></i>:<i class="fa fa-plus-circle fa-2x" onClick={this.onClickAddID}></i>}
            </div>
          </div>
          <div className="option-product">
            <div className="preview-option">
              <Select style={{ width: '100px' }} className="filter" defaultValue="Custom" onChange={this.onChangeContentID}>
                <Option value="Sentence">Sentence</Option>
                <Option value="Alphabet">Alphabet</Option>
                <Option value="Paragraph">Paragraph</Option>
                <Option value="Numerals">Numerals</Option>
                <Option value="Custom">Custom</Option>
              </Select>
            </div>
            <div className="style-option">
              <Select style={{ width: '100px' }} className="filter" defaultValue="Custom" onChange={this.onChangePreview}>
                <Option value="Sentence">Sentence</Option>
                <Option value="Alphabet">Alphabet</Option>
                <Option value="Paragraph">Paragraph</Option>
                <Option value="Numerals">Numerals</Option>
                <Option value="Custom">Custom</Option>
              </Select>
            </div>
            <div className="size-slider">
              <Slider style={{ width: 95, }} defaultValue={32} max="248" onChange={this.onChangeSizeSliderID} />
            </div>
          </div>
          <div className="product-detail">
            <TextArea style={{ 'font-size': font.fontSize, 'font-family': font.family }} value={font.content} autosize={{ minRows: 6, maxRows: 6 }} />
          </div>
        </div>
      </Col>
    </>
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Product);